﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMovement : MonoBehaviour {

    public float moveSpeed;
    public float jumpSpeed;
    bool didJump = false;
    private Rigidbody2D charRigidBody;

    public bool isGround;
    public LayerMask whatIsGround;

    private Collider2D charCollider;

    void Start()
    {
        charRigidBody = GetComponent<Rigidbody2D>();
        charCollider = GetComponent<Collider2D>();
    }

	void Update () {
		if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            didJump = true;
        }
	}
    //DO physics engine updates here
    void FixedUpdate()
    {
        isGround = Physics2D.IsTouchingLayers(charCollider, whatIsGround);
        charRigidBody.velocity = new Vector2(moveSpeed, charRigidBody.velocity.y);

        if (didJump)
        {
            didJump = false;
            if (isGround) {
                charRigidBody.velocity = new Vector2(charRigidBody.velocity.x, jumpSpeed);
            }
        }
    }
}
