﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGLooper : MonoBehaviour {

    int numBGPanels = 3;
    public GameObject trashBin;

	void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Triggred: " + collider.name);

        float widthOfBGObjects = ((BoxCollider2D)collider).size.x;

        Debug.Log("Size: " + widthOfBGObjects);

        Vector3 pos = collider.transform.position;
        pos.x += widthOfBGObjects * numBGPanels - widthOfBGObjects/2.464f;
        collider.transform.position = pos;
    }
}
